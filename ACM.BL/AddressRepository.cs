﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACM.BL
{
    public class AddressRepository
    {
        public AddressRepository()
        {
        }

        public Address Retrieve(int addressId)
        {
            Address address = new Address(addressId);

            if (addressId == 1)
            {
                address.StreetLine1 = "Bag End";
                address.StreetLine2 = "Bagshot Row";
                address.City = "Hobbiton";
                address.State = "Shire";
                address.Country = "Middle Earth";
                address.PostalCode = "144";
            }
            return address;
        }

        public IEnumerable<Address> RetrieveByCustomerId(int customerId)
        {
            var addressList = new List<Address>();
            Address address = new Address(1);

			address.StreetLine1 = "Bag End";
			address.StreetLine2 = "Bagshot Row";
			address.City = "Hobbiton";
			address.State = "Shire";
			address.Country = "Middle Earth";
			address.PostalCode = "144"; 

            addressList.Add(address);

            address = new Address(2);
			address.StreetLine1 = "Green Dragon";
			address.City = "By Water";
			address.State = "Shire";
			address.Country = "Middle Earth";
			address.PostalCode = "146";

            addressList.Add(address);

            return addressList;

		}


        public bool Save(Address address)
        {

            return true;
        }

    }//endof class
}
