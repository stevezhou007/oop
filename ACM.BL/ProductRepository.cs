﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACM.BL
{
    public class ProductRepository
    {
        public ProductRepository()
        {
        }

		public Product Retrieve(int productId)
		{
            Product product = new Product(productId);

            if (productId == 2)
            {
                product.ProductName = "sunflowers";
                product.ProductDescription = "Assorted Size";
                product.CurrentPrice = 15.96M;
            }
            return product;
        }

        public bool Save(Product product)
		{
            var success = true;

            if (product.hasChanges && product.IsValid)
            {
                if (product.IsNew)
                {
                    
                }
                else 
                {
                    
                }
            }
            return success;
        }

    }// end of class
}
