﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ACM.BL
{
    public class OrderRepository
    {
        public OrderRepository()
        {
        }

		public Order Retrieve(int orderId)
		{
            Order order = new Order(orderId);

            if (orderId == 10)
            {
                order.OrderDate = new DateTimeOffset();
            }
            return order;
        }

		public bool Save(Order order)
		{
			return true;
		}

        public OrderDisplay RetrieveOrderDisplay(int orderId)
        {
            OrderDisplay orderDisplay = new OrderDisplay();

            if (orderId == 10)
            {
                orderDisplay.FirstName = "Bilbo";
                orderDisplay.LastName = "Baggins";

                orderDisplay.OrderDate = new DateTimeOffset(2014, 4, 14, 10, 00, 00, new TimeSpan(7,0,0));
                orderDisplay.ShippingAddress = new Address(1);
                orderDisplay.ShippingAddress.StreetLine1 = "Bag End";
                orderDisplay.ShippingAddress.StreetLine2 = "Bagshot row";
                orderDisplay.ShippingAddress.City = "Hobbiton";
                orderDisplay.ShippingAddress.State = "Shire";
                orderDisplay.ShippingAddress.Country = "Middle Earth";
                orderDisplay.ShippingAddress.PostalCode = "144";
            }

            orderDisplay.orderDisplayItemList = new List<OrderDisplayItem>();

            if (orderId == 10)
            {
                var orderDisplayItem = new OrderDisplayItem();
                orderDisplayItem.ProductName = "Sunflowers";
                orderDisplayItem.PurchasePrice = 15.9M;
                orderDisplayItem.OrderQuantity = 2;

                orderDisplay.orderDisplayItemList.Add(orderDisplayItem);

                orderDisplayItem = new OrderDisplayItem();
                orderDisplayItem.ProductName = "Rake";
                orderDisplayItem.PurchasePrice = 6M;
                orderDisplayItem.OrderQuantity = 1;

                orderDisplay.orderDisplayItemList.Add(orderDisplayItem);
            }

            return orderDisplay;
        }


    }//end of class
}
